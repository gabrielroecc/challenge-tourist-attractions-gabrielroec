export class TouristApp {
    constructor() {
        this.list = [];
        this.selectors();
        this.events();
    }

    selectors() {
        this.itemImg = document.getElementById("file-image");
        this.itemPrev = document.querySelector(".image-preview");
        this.inputTitle = document.querySelector(".form-input-title");
        this.inputDesc = document.querySelector(".form-input-description");
        this.form = document.querySelector(".tourist-form");
        this.places = document.querySelector(".places");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));
        this.itemImg.addEventListener("change", this.previewImage.bind(this));
    }

    // Preview da img
    previewImage() {
        const reader = new FileReader();
        let imagem = this.itemPrev;
        reader.addEventListener("load", function () {
            imagem.src = reader.result;
        });
        // read the image file as a data URL.
        reader.readAsDataURL(this.itemImg.files[0]);
    }
    // Preview da img

    addItemToList(event) {
        event.preventDefault();

        const prevCards = this.itemPrev.src;
        const itemTitle = event.target["title"].value;
        const itemDescription = event.target["description"].value;

        const item = {
            image: prevCards,
            title: itemTitle,
            desc: itemDescription,
        };

        this.list.push(item);
        this.renderListItems();
        this.formReset();
    }

    renderListItems() {
        let itemsStructure = "";
        this.list.forEach(function (item) {
            itemsStructure += `
                    <li class = "result-places">

                        <div class="result-image-wrapper">
                            <img class="renderized-img" src="${item.image}"></img>
                        </div>

                        <div class="card-result">
                            <h1>${item.title}</h1>
                            <p>${item.desc}</p>
                        </div>

                    </li>
                `;
        });

        this.places.innerHTML = itemsStructure;
    }

    formReset() {
        this.inputTitle.value = "";
        this.inputDesc.value = "";
        document.querySelector(".image-preview").setAttribute("src", "#");
    }
}
